FROM archlinux

RUN \
	pacman -Sy \
		--noconfirm \
			sudo \
&& \
	mkdir -p /home/auser \
&& \
	useradd \
		--home-dir /home/auser \
		--gid users \
		--password "x" \
		auser \
&& \
	chown auser:users /home/auser \
&& \
	echo 'auser ALL=NOPASSWD: ALL' >> /etc/sudoers
