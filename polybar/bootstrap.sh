#!/usr/bin/env sh

if ! test $(command -v polybar)
then
	dirname=/tmp/polybar

	if [ -d $dirname ]; then
		rm -rf $dirname
	fi

	git clone https://aur.archlinux.org/polybar.git $dirname
	(cd $dirname && makepkg -si)

	rm -rf $dirname
fi
