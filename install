#!/usr/bin/env bash
#
# bootstrap installs things.

cd "$(dirname "$0")"
DOTFILES_ROOT=$(pwd -P)

set -e

echo ''

source misc/output.sh

remove_unwanted_dependencies () {
  local sudo_cmd=""
  if [[ $EUID -ne 0 ]]; then
    sudo_cmd="sudo"
  fi

  $sudo_cmd pacman -R nitrogen || true
}

install_pacman_requirements () {
  RED='\033[0;34m'
  NC='\033[0m' # No Color

  local file=/tmp/dotfiles_pacman_requirements.txt

  if [ -f $file ]; then
    rm $file
    touch $file
  fi

  for requirements in **/packages.pacman; do
    info "reading requirements in: ${requirements}"
    cat $requirements >> $file
  done

  SUDO_CMD=""
  if [[ $EUID -ne 0 ]]; then
    SUDO_CMD="sudo"
  fi

  readarray -t pacman_pkgs < $file

  info "Installing: ${pacman_pkgs[*]}"

  printf "${RED}"
  $SUDO_CMD pacman -Syu
  $SUDO_CMD pacman -S --needed ${pacman_pkgs[@]}

  rm $file
  success "pacman Installation complete"
}

install_baph () {
  local sudo_bin=""
  if [[ $EUID -ne 0 ]]; then
    sudo_bin="sudo"
  fi

  chmod +x misc/baph && ${sudo_bin} cp misc/baph /usr/bin/baph
}

install_aur_requirements () {
  RED='\033[0;34m'
  NC='\033[0m' # No Color

  local file=/tmp/dotfiles_aur_requirements.txt
  local aur_bin=baph

  install_baph

  if [ -f $file ]; then
    rm $file
    touch $file
  fi

  for requirements in **/packages.aur; do
    info "reading requirements in: ${requirements}"
    cat $requirements >> $file
  done

  readarray -t aur_pkgs < "$file"

  info "Installing: ${aur_pkgs[*]}"

  printf "${RED}"
  ${aur_bin} -i -n -N "${aur_pkgs[@]}"

  rm $file
  success "AUR Installation complete"
}

run_bootstrap () {
  RED='\033[0;34m'
  NC='\033[0m' # No Color

  for installer in **/bootstrap.sh; do
    info "bootstraping ${installer}"
    printf "${RED}"
    sh -c "${installer}"
    printf "${NC}"
  done
}

link_file () {
  local src=$1 dst=$2

  local overwrite= backup= skip=
  local action=

  if [ -f "$dst" -o -d "$dst" -o -L "$dst" ]
  then

    if [ "$overwrite_all" == "false" ] && [ "$backup_all" == "false" ] && [ "$skip_all" == "false" ]
    then

      local currentSrc="$(readlink $dst)"

      if [ "$currentSrc" == "$src" ]
      then

        skip=true;

      else

        user "File already exists: $dst ($(basename "$src")), what do you want to do?\n\
        [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
        read -n 1 action

        case "$action" in
          o )
            overwrite=true;;
          O )
            overwrite_all=true;;
          b )
            backup=true;;
          B )
            backup_all=true;;
          s )
            skip=true;;
          S )
            skip_all=true;;
          * )
            ;;
        esac

      fi

    fi

    overwrite=${overwrite:-$overwrite_all}
    backup=${backup:-$backup_all}
    skip=${skip:-$skip_all}

    if [ "$overwrite" == "true" ]
    then
      rm -rf "$dst"
      success "removed $dst"
    fi

    if [ "$backup" == "true" ]
    then
      mv "$dst" "${dst}.backup"
      success "moved $dst to ${dst}.backup"
    fi

    if [ "$skip" == "true" ]
    then
      success "skipped $src"
    fi
  fi

  if [ "$skip" != "true" ]  # "false" or empty
  then
    parent=$(dirname "$2")
    if [ ! -d "$parent" ]
    then
      mkdir -p "$parent"
    fi
    ln -s "$1" "$2"
    success "linked $1 to $2"
  fi
}

install_dotfiles () {
  info 'installing dotfiles'

  local overwrite_all=false backup_all=false skip_all=false

  for src in $(find -H "$DOTFILES_ROOT" -maxdepth 2 -name '*.symlink' -not -path '*.git*')
  do
    dst=$(basename "${src%.*}")
    dst="$HOME/.${dst//+//}"
    link_file "$src" "$dst"
  done
}

remove_unwanted_dependencies 
install_pacman_requirements
install_aur_requirements
run_bootstrap
install_dotfiles

echo ""
success "All installed!"
