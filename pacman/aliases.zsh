#!/usr/bin/env zsh

if test $(command -v pacman)
then
    alias pup='sudo pacman -Syyu' # update
    alias pin='sudo pacman -S'    # install
    alias pun='sudo pacman -Rs'   # remove
    alias pcc='sudo pacman -Scc'  # clear cache
    alias pls='pacman -Ql'        # list files
    alias prm='sudo pacman -R --nosave --recursive' # really remove, configs and all
fi
