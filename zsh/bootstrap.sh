SUDO_CMD=""
if [[ $EUID -ne 0 ]]; then
	SUDO_CMD="sudo"
fi

$SUDO_CMD chsh -s `cat /etc/shells | grep zsh | head -1`
