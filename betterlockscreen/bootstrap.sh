#!/usr/bin/env sh

if ! test $(command -v betterlockscreen)
then
	dirname=/tmp/betterlockscreen

	if [ -d $dirname ]; then
		rm -rf $dirname
	fi

	git clone https://aur.archlinux.org/betterlockscreen.git $dirname
	(cd $dirname && makepkg -si)

	rm -rf $dirname
fi

betterlockscreen -u feh/wallpaper.png
