source misc/output.sh

FONT_DIR="$HOME/.local/share/fonts/"
GIT_REPO=https://github.com/stark/siji.git
TMP_DIR="/tmp/siji"

function isInstalled() {
	fc-list | grep "siji.pcf" > /dev/null
	if [ 0 != $? ]; then
		return 1
	fi

	return 0
}

function install() {
	local dirname="$TMP_DIR"
	if [ -d "$dirname" ]; then
		rm -rf $dirname
	else
		mkdir -p $dirname
	fi

	local fontdir=$FONT_DIR
	if [ ! -d $fontdir ]; then
		mkdir -p $fontdir
	fi

	git clone $GIT_REPO $dirname
	(cd $dirname && ./install.sh -d "$FONT_DIR")

	isInstalled
	if [ 0 == $? ]; then
		success "siji installed"
	else
		error "Couldnt find Siji"
	fi
}

isInstalled
if [ 0 != $? ]; then
	install
fi
