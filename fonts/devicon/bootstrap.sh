source misc/output.sh

FONT_DIR="$HOME/.local/share/fonts/"
GIT_REPO=https://github.com/vorillaz/devicons.git
TMP_DIR=/tmp/devicon

function isInstalled() {
	fc-list | grep "devicons.ttf" > /dev/null
	if [ 0 != $? ]; then
		return 1
	fi

	return 0
}

function install() {
	local dirname=$TMP_DIR
	if [ -d $dirname ]; then
		rm -rf $dirname
	else
		mkdir -p $dirname
	fi

	local fontdir=$FONT_DIR
	if [ ! -d $fontdir ]; then
		mkdir -p $fontdir
	fi

	git clone "$GIT_REPO" "$dirname"
	(cd $dirname && cp fonts/devicons.ttf $fontdir)

	# reload cache
	info "Reload font cache"
	fc-cache -f -v 2>&1 > /dev/null

	isInstalled
	if [ 0 == $? ]; then
		success "Devicons installed"
	else
		error "Couldnt find Devicons"
	fi
}

isInstalled
if [ 0 != $? ]; then
	install
fi
