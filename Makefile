.PHONY: sh
sh: build
	docker \
		run \
			--rm \
			-ti \
			-v $(PWD)/tmp/.pacman:/var/cache/pacman/pkg \
			-v $(PWD):/home/auser/.dotfiles \
			-w /home/auser \
			-u auser:users \
			tomtomsen:dotfiles \
			bash

build:
	docker \
		build \
			--pull \
			--tag tomtomsen:dotfiles \
			.
