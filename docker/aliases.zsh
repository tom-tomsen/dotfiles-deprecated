#!/usr/bin/env zsh

alias d='docker $*'
alias d-c='docker-compose $*'
alias dockerx='docker run --rm -ti -v $PWD:$PWD -w $PWD $*'
