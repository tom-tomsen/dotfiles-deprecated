#!/usr/bin/env sh

SUDO_CMD=""
if [[ $EUID -ne 0 ]]; then
	SUDO_CMD="sudo"
fi

$SUDO_CMD usermod -a -G docker `whoami`

if pidof systemd > /dev/null
then
	$SUDO_CMD systemctl enable docker
	$SUDO_CMD systemctl start docker
fi
