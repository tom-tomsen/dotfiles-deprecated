#!/usr/bin/env zsh

alias q='exit 0'
alias cls='clear' # Good 'ol Clear Screen command

alias la='LANG=C ls -1lAh --group-directories-first --color'
alias ll='ls -lAh'
alias l.='ls -ld .*'

alias mkdir='mkdir -pv'
alias grep='grep --color=auto'
alias debug="set -o nounset; set -o xtrace"
alias x='chmod +x'

alias mine!='sudo chown -R $UID:$GID'
