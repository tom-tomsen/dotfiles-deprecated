CURR_USER=$USER
FILE="/etc/sudoers.d/$CURR_USER"

echo "$CURR_USER ALL=(ALL) NOPASSWD:ALL" | sudo tee "$FILE"
