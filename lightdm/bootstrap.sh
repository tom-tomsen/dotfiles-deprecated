#!/bin/sh
FILE=/etc/lightdm/lightdm.conf.bak

if [ -f $FILE ]; then
    sudo sed -i 's/#autologin-guest=.*/autologin-guest=false/g' "$FILE"
    sudo sed -i 's/#autologin-user=.*/autologin-user='"$USER"'/g' "$FILE"
    sudo sed -i 's/#autologin-user-timeout=.*/autologin-user-timeout=0/g' "$FILE"
    
    sudo groupadd -r autologin
    sudo gpasswd -a $USER autologin
fi