source $HOME/bin/antigen.zsh

antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle https://github.com/robbyrussell/oh-my-zsh.git plugins/z
antigen theme denysdovhan/spaceship-prompt

antigen apply

if [ ! -f $HOME/.z ]
then
	touch $HOME/.z
fi
