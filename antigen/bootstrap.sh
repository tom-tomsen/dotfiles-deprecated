BIN_DIR=$HOME/bin

if [ ! -d $BIN_DIR ]; then
	mkdir -p $BIN_DIR
fi

curl -L git.io/antigen > $BIN_DIR/antigen.zsh
